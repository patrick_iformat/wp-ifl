<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'odina_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'D2CUT9d}`W#[S`Jxku).~xFb,+3pme^PAHn$!RWeJQ245?F4p^V1a5KVo`PnVulh' );
define( 'SECURE_AUTH_KEY',  'w61u&}K1#2uojmru5Zttls<AXIi:t3aSo5`hVR|^1;KH$//o_l*Do9L.Yq,?/I1u' );
define( 'LOGGED_IN_KEY',    'fD|=@s*uY r>Iuv(>,kQV5r-sDw&-1q^L`xU^)7Qn.6QcvlxwwTng>amp/ J+v{,' );
define( 'NONCE_KEY',        'Zjdy.fG+DJFM]#jfA8|O2z_8w>3DOO3Ij!XvPdT._0xi^~|vA%iT|2:j9o890izt' );
define( 'AUTH_SALT',        'E_(|`B{KqNJYb*k<PLh*5cZlaK?_MI0cKV{H$tx8FeqCOyM=gwr[XR/Fm_<D-2:2' );
define( 'SECURE_AUTH_SALT', 'xoyto6<#]+>A87Vzxv&t1Fzl9_~/qtzeD7C=$V2}d^2cx^i0|B)MG;;,+9iZQQE`' );
define( 'LOGGED_IN_SALT',   ')!:$tJqn6(I}] bj&ZvjcJ8nnt!4&9j-;m0/#da|ubv<_2QB+&Ttlk42tmB*lWYu' );
define( 'NONCE_SALT',       '5SN-!Th%Qcpp[&f!4D`mV7(.rtAl#}|ZFrCb&SPB),PRDZrXY^)~Ut;yh<7]d)^O' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
