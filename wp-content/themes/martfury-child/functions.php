<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

/*Extra field on the seller settings -Dokan*/

// Add seller_whatsapp_number field in seller settings

// add_filter( 'event_manager_event_listing_data_fields', 'admin_add_country_field' );
// function admin_add_country_field( $fields ) {
// 	$fields['_event_country'] = array(
// 	  'label'       => __( 'Country ($)', 'event_manager' ),
// 	  'type'        => 'text',
// 	  'placeholder' => 'e.g. germmany',
// 	  'description' => ''
// 	);
// 	return $fields;
// }

// add_filter( 'event_manager_event_listing_data_fields', 'admin_add_vendor_field' );

// function admin_add_vendor_field( $fields ) {

// 	global $wpdb;

// 	$results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}users WHERE user_nicename = 'vendor'", OBJECT );

// 	$options = [];

// 	foreach($results as $key => $result){
// 		$options[$result->ID] = $result->display_name;
// 	}

//   $fields['_event_vendor'] = array(
//     'label'       => __( 'Vendor', 'event_manager' ),
// 	'type'        => 'select',
// 	'multiple' 	  => true,	
// 	'class' 	  => 'input-select event-manager-category-dropdown',
//     'required'    => true,
// 	'placeholder' => 'vendor',
// 	'options' => $options
//   );
//   return $fields;
// }

// add_filter( 'event_manager_event_listing_data_fields', 'admin_add_vendor_products_field' );

// function admin_add_vendor_products_field( $fields ) {

// 	global $wpdb;

// 	$results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}users WHERE user_nicename = 'vendor'", OBJECT );

// 	$options = [];

// 	foreach($results as $key => $result){
// 		$options[$result->ID] = $result->display_name;
// 	}

//   $fields['_event_vendor_products'] = array(
//     'label'       => __( 'Vendor Products', 'event_manager' ),
// 	'type'        => 'select',
//     'required'    => true,
// 	'placeholder' => 'Vendor Products',
// 	'options' => $options
//   );
//   return $fields;
// }


// add_filter( 'dokan_settings_form_bottom', 'seller_whatsapp_number', 10, 2);

// function seller_whatsapp_number( $current_user, $profile_info ){
// 	$seller_whatsapp_number= isset( $profile_info['seller_whatsapp_number'] ) ? $profile_info['seller_whatsapp_number'] : '';
// 	?>
<!-- // 	<div class="gregcustom dokan-form-group">
// 		<label class="dokan-w3 control-label" for="reg_seller_whatsapp_number">
// 			<?php //_e( 'WhatsApp Number', 'dokan' ); ?>
// 		</label>
// 		<div class="dokan-w5">
// 			<input type="text" class="dokan-form-control input-md valid" name="seller_whatsapp_number" id="reg_seller_whatsapp_number" value="<?php //cho $seller_whatsapp_number; ?>" />
// 		</div>
// 	</div> -->
 	<?php
// }

// Add seller_whatsapp_number field in seller settings

add_filter( 'dokan_settings_form_bottom', 'whatsapp_cta_display_on_page', 10, 2);

function whatsapp_cta_display_on_page( $current_user, $profile_info ){

	$whatsapp_cta_display_on_page = isset( $profile_info['whatsapp_cta_display_on_page'] ) ? $profile_info['whatsapp_cta_display_on_page'] : '';
	?>
	<div class="gregcustom dokan-form-group">
		<label class="dokan-w3 dokan-control-label" for="reg_whatsapp_cta_display_on_page">
			<?php _e( 'WhatsApp CTA Display on page', 'dokan' ); ?>
		</label>
		<div class="dokan-w5 dokan-text-left dokan_tock_check">
			<div class="checkbox" style="display: flex">
				<label style="margin-right: 10px">
					<input type="radio" id="whatsapp_cta_display_on_page_enable" <?php echo ($whatsapp_cta_display_on_page === 'yes') ? 'checked' : '' ;?> value="yes" name="whatsapp_cta_display_on_page">
						 Yes
				</label>
				<label>
					<input type="radio" id="whatsapp_cta_display_on_page_disable" <?php echo ($whatsapp_cta_display_on_page === 'no') ? 'checked' : '' ;?> value="no" name="whatsapp_cta_display_on_page">
					 No
				</label>
			</div>
		</div>

	</div>
	<?php
}

//save the field value

add_action( 'dokan_store_profile_saved', 'save_seller_whatsapp_number', 15 );
function save_seller_whatsapp_number( $store_id ) {
	$dokan_settings = dokan_get_store_info($store_id);
	if ( isset( $_POST['seller_whatsapp_number'] ) ) {
		$dokan_settings['seller_whatsapp_number'] = $_POST['seller_whatsapp_number'];
	}
	update_user_meta( $store_id, 'dokan_profile_settings', $dokan_settings );
}

add_action( 'dokan_store_profile_saved', 'save_whatsapp_cta_display_on_page', 15 );
function save_whatsapp_cta_display_on_page( $store_id ) {
	$dokan_settings = dokan_get_store_info($store_id);
	if ( isset( $_POST['whatsapp_cta_display_on_page'] ) ) {
		$dokan_settings['whatsapp_cta_display_on_page'] = $_POST['whatsapp_cta_display_on_page'];
	}
	update_user_meta( $store_id, 'dokan_profile_settings', $dokan_settings );
}

add_action('wp_head', 'showHideButton');
function showHideButton(){
    if( is_product() )
    {
	    $product = wc_get_product( get_the_ID());

	    $vendor_id = get_post_field( 'post_author', $product->get_id() );

	    // Get Vendor Profile settings
	    $profile_settings = get_user_meta( $vendor_id, 'dokan_profile_settings' );

	    $display_on_page = $profile_settings[0]['whatsapp_cta_display_on_page'];
	    $vendor_whatsapp_number = $profile_settings[0]['seller_whatsapp_number'];

	    if ( $display_on_page === 'no'){
	        echo '
	            <script>
	                jQuery(document).ready(function(){
	                    jQuery(".mvvwo_cart_button").html(""); 
	                })
	            </script>
	        ';
        }else{
		    $replaces = [
			    '{PRODUCT_NAME}' => $product->get_name(),
			    '{PRODUCT_URL}' => get_permalink($product->get_id()),
			    '{PRODUCT_PRICE}' => oww_price($product->get_price()),
		    ];

		    echo '
	            <script>
	                var vendor_whatsAppLink = function (mobile, message) {
                        mobile = mobile.replace(/[^0-9]/g, \'\');
                        return \'https://api.whatsapp.com/send?phone=\' + mobile + \'&text=\' + encodeURI(message);
                    }
    
	                jQuery(document).ready(function ($) {
                        document.querySelectorAll(".mvvwo_cart_button a").forEach(function (link) {
                        link.addEventListener("click", function (event) {
            
                            var mvv_message = link.getAttribute("data-message");
                            mvv_message = mvvwo_replace(mvv_message, link);
                            link.setAttribute("data-vendorNumber", '. $vendor_whatsapp_number .')
            
//                            link.setAttribute("href", mvv_whatsAppLink(mvvwo_numbersAvailable.number, mvv_message));
                            link.setAttribute("href", vendor_whatsAppLink(link.getAttribute("data-vendorNumber"), mvv_message));
            
                        })
                        // var mvv_message = link.getAttribute(\'data-message\');
                        //  link.setAttribute(\'href\', mvv_whatsAppLink(mvvwo_numbersAvailable.number, mvv_message));
                    })
                    });
	            </script>
	        ';
        }
//	    echo '<pre>';
//	    var_dump(get_user_meta( $vendor_id, 'dokan_profile_settings' ));
//	    echo '</pre>';

    }
}